<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bssimplex_description' => 'Mini and minimalist',
	'theme_bssimplex_slogan' => 'Mini and minimalist',
);
